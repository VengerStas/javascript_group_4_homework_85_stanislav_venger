import React, { Component } from 'react';
import './App.css';
import {Route, Switch} from "react-router-dom";
import Artists from "../components/Artists/Artists";
import Albums from "../components/Albums/Albums";
import Tracks from "../components/Tracks/Tracks";

class App extends Component {
  render() {
    return (
      <Switch>
        <Route path='/' exact component={Artists}/>
        <Route path='/album/:id/:artist' exact component={Albums}/>
        <Route path='/track/:id/:artist/:album' exact component={Tracks}/>
      </Switch>
    );
  }
}

export default App;
