import React, {Component} from 'react';
import {fetchGetTracks} from "../../store/actions";
import {connect} from "react-redux";
import {NavLink} from "react-router-dom";

import './Tracks.css';

class Tracks extends Component {
    componentDidMount() {
        this.props.loadTracks(this.props.match.params.id);
    }


    render() {
        let track = this.props.tracks.map(id => {
            return (
                <div>
                    <div className="track-item"><p>{id.number}</p><p>{id.name}</p><p>{id.time}</p></div>
                </div>
            )
        });
        return (
            <div>
                <NavLink to="/">Artist list</NavLink>
                <h4 className="tracks-title">Track list</h4>
                <div className="track list">
                    <p className="track-album-name">Album name: <span className="album-title">{this.props.match.params.artist}/{this.props.match.params.album}</span></p>
                    {track}
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    tracks: state.tracks,
});

const mapDispatchToProps = dispatch => ({
    loadTracks: id => dispatch(fetchGetTracks(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Tracks);