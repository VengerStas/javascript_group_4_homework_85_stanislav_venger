import React from 'react';
import {CardImg} from "reactstrap";


const AlbumThumbnail = props => {
    if (props.image !== "null") {
        let image = 'http://localhost:8000/uploads/album/' + props.image;
        return <CardImg src={image} className="img-thumbnail" alt="News Image" />;
    }
    return null;
};

export default AlbumThumbnail;