import React, {Component} from 'react';
import {connect} from "react-redux";
import {fetchGetArtists} from "../../store/actions";
import {Card, CardBody, CardLink, CardText, CardTitle} from "reactstrap";
import ImageThumbnail from "../ImageThumbnail/ImageThumbnail";

import './Artists.css';

class Artists extends Component {
    componentDidMount() {
        this.props.loadArtist();
    };
    render() {
        let artist = this.props.artists.map(id => {
            return (
                <Card key={id._id} className="artist-card">
                    <CardBody>
                        <CardTitle>{id.name}</CardTitle>
                    </CardBody>
                    <ImageThumbnail image={id.photo}/>
                    <CardBody>
                        <CardText>{id.info}</CardText>
                        <CardLink href={`/album/${id._id}/${id.name}`}>Albums</CardLink>
                    </CardBody>
                </Card>
            )
        });
        return (
            <div>
                <h4>Artists list</h4>
                <div className="artist-list">
                    {artist}
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    artists: state.artists,
});

const mapDispatchToProps = dispatch => ({
    loadArtist: () => dispatch(fetchGetArtists()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Artists);
