import {ALBUM_REQUEST_SUCCESS, ARTISTS_REQUEST_SUCCESS, TRACK_REQUEST_SUCCESS} from "./actions";

const initialState = {
    artists: [],
    albums: [],
    tracks: []
};

const reducers = (state = initialState, action) => {
    switch (action.type){
        case ARTISTS_REQUEST_SUCCESS:
            return {
                ...state,
                artists: action.artists
            };
        case ALBUM_REQUEST_SUCCESS:
            return {
                ...state,
                albums: action.albums
            };
        case TRACK_REQUEST_SUCCESS:
            return {
                ...state,
                tracks: action.tracks
            };
        default:
            return state;
    }
};


export default reducers;