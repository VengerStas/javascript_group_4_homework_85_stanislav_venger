const express = require('express');
const mongoose = require ('mongoose');
const config = require('./config');
const cors = require ('cors');
const artist = require('./app/artist');
const album = require ('./app/album');
const track = require ('./app/track');
const users = require('./app/user');
const trackHistory = require('./app/trackHistory');

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const port = 8000;

mongoose.connect(config.dbUrl, config.mongoOption).then(() => {
   app.use('/artist', artist);
   app.use('/album', album);
   app.use('/track', track);
   app.use('/users', users);
   app.use('/track_history', trackHistory);
    app.listen(port, () => {
        console.log(`Server started on ${port} port`);
    });
});